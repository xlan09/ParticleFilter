# Particle Filter
- Simple implementation of particle filter.

# Gui
- Cyan dot is the estimated robot position from particle filter.

![Gui pic does not show properly, go to files and click Gui.png to view!](Gui.png)

# Dependencies:
- python2.7
- numpy
- Scipy
- pygame
