#!/usr/bin/env python

import pygame
import numpy as np
from time import sleep
import random
from scipy.stats import multivariate_normal


class Obstacle(object):
    """
    Rectangular obstacles
    """

    def __init__(self, ne_corner, sw_corner):
        if len(ne_corner) != 2 or len(sw_corner) != 2 or ne_corner[0] <= sw_corner[0] or ne_corner[1] <= sw_corner[1]:
            raise ValueError('Not valid corner specifications!')

        self.se_corner = np.array(ne_corner)
        self.nw_corner = np.array(sw_corner)

    def is_in_obs(self, pos, cushion = 0):
        if len(pos) < 2:
            raise ValueError('Not valid arguments when checking whether position is inside obstacle')

        return self.nw_corner[0] - cushion <= pos[0] <= self.se_corner[0] + cushion\
               and self.nw_corner[1] - cushion <= pos[1] <= self.se_corner[1] + cushion

    def draw(self, screen):
        pygame.draw.rect(
            screen,
            [0, 0, 0],
            pygame.Rect(self.nw_corner[0],
                        self.nw_corner[1],
                        self.se_corner[0] - self.nw_corner[0],
                        self.se_corner[1] - self.nw_corner[1])
        )


class World(object):
    """
    World
    Coordinate system: origin is in the top left corner, x axis is horizontal pointing right, y axis is vertical
    pointing down
    """

    def __init__(self, sw_corner, ne_corner):
        if len(ne_corner) != 2 or len(sw_corner) !=2 or ne_corner[0] <= sw_corner[0] or ne_corner[1] <= sw_corner[1]:
            raise ValueError('Not valid corner specifications!')

        self.se_corner = np.array(ne_corner)
        self.nw_corner = np.array(sw_corner)
        self.obstacles = []
        self.__init_obs()

    def __init_obs(self):
        """
        Manually choose obstacle positions
        """

        base_length= (1.0 / 10) * min(self.se_corner[0] - self.nw_corner[0], self.se_corner[1] - self.nw_corner[1])
        obs_sw_corner = np.array(self.nw_corner)

        # Add obstacles
        horizontal_obs = Obstacle(obs_sw_corner + np.array([2 * base_length, base_length]), obs_sw_corner)

        transition = np.array([2 * base_length, 2 * base_length])
        self.obstacles.append(Obstacle(horizontal_obs.se_corner + transition, horizontal_obs.nw_corner + transition))

        transition = np.array([2 * base_length, 6 * base_length])
        self.obstacles.append(Obstacle(horizontal_obs.se_corner + transition, horizontal_obs.nw_corner + transition))

        vertical_obs = Obstacle(obs_sw_corner + np.array([base_length, 2 * base_length]), obs_sw_corner)

        transition = np.array([6 * base_length, 1 * base_length])
        self.obstacles.append(Obstacle(vertical_obs.se_corner + transition, vertical_obs.nw_corner + transition))

        transition = np.array([8 * base_length, 3 * base_length])
        self.obstacles.append(Obstacle(vertical_obs.se_corner + transition, vertical_obs.nw_corner + transition))

        transition = np.array([6 * base_length, 7 * base_length])
        self.obstacles.append(Obstacle(vertical_obs.se_corner + transition, vertical_obs.nw_corner + transition))

    def is_free(self, pos, cushion = 0):
        if not (self.nw_corner[0] + cushion <= pos[0] <= self.se_corner[0] - cushion
                and self.nw_corner[1] + cushion <= pos[1] <= self.se_corner[1] - cushion):
            return False

        for obs in self.obstacles:
            if obs.is_in_obs(pos, cushion):
                return False

        return True

    def init_screen(self):
        pygame.init()
        screen = pygame.display.set_mode(np.int32([self.se_corner[0] - self.nw_corner[0], self.se_corner[1] - self.nw_corner[1]]))
        pygame.display.set_caption('Cluttered World')

        screen.fill((255, 255, 255))
        pygame.display.flip()
        return screen

    def draw(self, screen, estimated_pos, robot):
        screen.fill((255, 255, 255))
        robot.draw(screen)

        # draw estimated position from particle filter
        pygame.draw.circle(screen, [0, 255, 255], np.int32(estimated_pos), 8)

        for obs in self.obstacles:
            obs.draw(screen)

        pygame.display.flip()

    def random_free_pos(self):
        width, height = self.se_corner - self.nw_corner
        while 1:
            random_pos = self.nw_corner + np.array([random.random() * width, random.random() * height])
            if self.is_free(random_pos):
                break

        return random_pos


class Robot(object):
    """
    Robot to localize
    """

    def __init__(self, world):
        self.robotWidth = 20
        self.robotHeight = 20
        self.pose = world.random_free_pos()
        self.last_pose = []
        self.world_size = world.se_corner - world.nw_corner
        self.control = 0 # current control applied

    def dynamics(self, curr_pose, control):
        """
        Robot dynamics
        """

        noise_mean = 0
        noise_deviation = 10

        # A simple dynamics
        return curr_pose + control + np.array([np.random.normal(noise_mean, noise_deviation),
                                                  np.random.normal(noise_mean, noise_deviation)])

    def update(self, world):
        """
        Update robot position
        :return:
        """

        max_input = 20

        while 1:
            random_heading = random.random() * 2 * np.pi
            control = np.array([np.cos(random_heading), np.sin(random_heading)]) * max_input
            if len(self.last_pose) == 0:
                break
            else:
                robot_heading = self.pose - self.last_pose
                if np.arccos(np.dot(control, robot_heading) / (np.linalg.norm(control) * np.linalg.norm(robot_heading))) \
                        < np.pi / 2 and world.is_free(self.dynamics(self.pose, control), 30):
                    break

        self.control = control
        self.last_pose = self.pose
        self.pose = self.dynamics(self.pose, self.control)
        self.pose = np.mod(self.pose, self.world_size)

    def draw(self, screen):
        # arrow to denote robot
        pygame.draw.rect(screen, [32,178,170], pygame.Rect(self.pose[0], self.pose[1], self.robotWidth, self.robotHeight))

        color = [255, 0, 0]
        wheel_radius = 5
        pygame.draw.circle(screen, color, np.int32(self.pose), wheel_radius)
        pygame.draw.circle(screen, color, np.int32(self.pose) + np.int32([self.robotWidth, 0]), wheel_radius)
        pygame.draw.circle(screen, color, np.int32(self.pose) + np.int32([0, self.robotHeight]), wheel_radius)
        pygame.draw.circle(screen, color, np.int32(self.pose) + np.int32([self.robotWidth, self.robotHeight]), wheel_radius)


class PositionSensor(object):
    """
    Sensor to detect robot position
    """

    def __init__(self, noise_deviation=10):
        self.noise_deviation = noise_deviation

    def noisy_output(self, position):
        """
        Give sensor about robot position
        """

        return self.true_output(position) + np.array([np.random.normal(0, self.noise_deviation),
                                                      np.random.normal(0, self.noise_deviation)])

    def true_output(self, position):
        return np.array(position)


class ParticleFilter(object):
    """
    Particle filter
    """

    def __init__(self, num_particles, system, sensor, world):
        """
        Particle filter
        :param num_particles: num of particles
        :param system: must contain API of dynamics
        :type: Robot
        :param sensor: sensor
        :type: Sensor
        """

        self.num_particles = num_particles
        self.particles = []
        self.weight = []
        for i in range(self.num_particles):
            self.particles.append(world.random_free_pos())
            self.weight.append(1.0 / self.num_particles)

        self.system = system
        self.sensor = sensor

    def filter(self):
        self._dynamics_update()
        self._observation_update()

        return np.array(self.weight).dot(np.array(self.particles))

    def _dynamics_update(self):
        """
        Dynamics update
        """
        new_particles = []
        for i in range(len(self.particles)):
            particle_index = np.random.choice(self.num_particles, p=self.weight)
            new_particles.append(self.system.dynamics(self.particles[particle_index], self.system.control))

        self.particles = new_particles

    def _observation_update(self):
        """
        Observation update
        """
        new_weights = []
        sensor_output = self.sensor.noisy_output(self.system.pose)
        cov_mat = np.diag(np.ones(len(sensor_output)) * self.sensor.noise_deviation)

        for i in range(len(self.weight)):
            particle_pos = self.sensor.true_output(self.particles[i])
            new_weights.append(multivariate_normal.pdf(sensor_output, particle_pos, cov_mat))

        self.weight = np.array(new_weights) / sum(new_weights)

if __name__ == '__main__':
    world = World([0, 0], [800, 800])
    screen = world.init_screen()
    robot = Robot(world)
    sensor = PositionSensor(noise_deviation=15)
    particle_filter = ParticleFilter(1000, robot, sensor, world)

    for i in range(100):
        robot.update(world)
        estimated_pos = particle_filter.filter()
        world.draw(screen, estimated_pos, robot)
        print('Estimated pos is {}'.format(estimated_pos))

    sleep(2)


